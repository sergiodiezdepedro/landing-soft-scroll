# Landing Site con soft scroll

Un ejemplo de landing site responsive, realizado con el enfoque *mobile-first*.

Mediante un sencillo script en Vanilla JavaScript se ejecuta un scroll suave. El script actúa con cualquier etiqueta `a` que conduzca a un lugar de la página con un ancla `#`.
